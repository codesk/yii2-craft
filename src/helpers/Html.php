<?php

namespace codesk\craft\helpers;

use kartik\helpers\Html as KartikHtml;
use yii\helpers\ArrayHelper;

class Html extends KartikHtml {

    /**
     * Get government year from specific year and month.
     * @param int $y Year. default : current year.
     * @param int $m Month. default : current month.
     * @return int
     */
    public static function getFiscalYear($y = null, $m = null) {
        $y = intval(isset($y) ? $y : date('Y'));
        $m = intval(isset($m) ? $m : date('n'));

        if ($m >= 10) {
            return $y + 1;
        }
        return intval($y);
    }

    /**
     * Convert year to Thai solar calendar year.
     * @param int $year Year.
     * @return int
     */
    public static function toBudYear($year) {
        return $year + 543;
    }

    /**
     * Font Awesome icon tag. (5.0 solid)
     * @param string $name Icon name.
     * @param array $options Tag options.
     * @return string
     */
    public static function awesome($name, $options = []) {
        $tag = ArrayHelper::remove($options, 'tag', 'span');
        $classPrefix = ArrayHelper::remove($options, 'prefix', 'fas fa-');
        static::addCssClass($options, $classPrefix . $name);
        return static::tag($tag, '', $options);
    }

}
