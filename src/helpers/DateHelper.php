<?php

namespace codesk\craft\helpers;

use DateTime;
use Exception;

class DateHelper {

    public static $months = [
        1 => 'มกราคม',
        2 => 'กุมภาพันธ์',
        3 => 'มีนาคม',
        4 => 'เมษายน',
        5 => 'พฤษภาคม',
        6 => 'มิถุนายน',
        7 => 'กรกฏาคม',
        8 => 'สิงหาคม',
        9 => 'กันยายน',
        10 => 'ตุลาคม',
        11 => 'พฤศจิกายน',
        12 => 'ธันวาคม',
    ];
    public static $shortMonths = [
        1 => 'ม.ค.',
        2 => 'ก.พ.',
        3 => 'มี.ค.',
        4 => 'เม.ย.',
        5 => 'พ.ค.',
        6 => 'มิ.ย.',
        7 => 'ก.ค.',
        8 => 'ส.ค.',
        9 => 'ก.ย.',
        10 => 'ต.ค.',
        11 => 'พ.ย.',
        12 => 'ธ.ค.',
    ];

    public static function getMonthOptions($m = null) {
        $ret = static::$months;
        if (isset($m)) {
            $m = intval($m);
            if (!isset($ret[$m])) {
                throw new Exception('invalid month value :' . $m);
            }
        }
        return $ret;
    }

    public static function getMonthShortOptions($m = null) {
        $ret = static::$shortMonths;
        if (isset($m)) {
            $m = intval($m);
            if (!isset($ret[$m])) {
                throw new Exception('invalid month value :' . $m);
            }
        }
        return $ret;
    }

    /**
     * Get Thailand's fiscal year by given date.
     * @param DateTime|string|int $date
     */
    public static function getYearFiscal($date = null) {
        $date = isset($date) ? $date : new DateTime;

        if (!$date instanceof DateTime) {
            $date = new DateTime($date);
        }

        if ($date instanceof DateTime) {
            $month = intval($date->format('n'));
            $year = intval($date->format('Y'));
        } else {
            $month = date('n');
            $year = date('Y');
        }

        if ($month >= 10) {
            return $year + 1;
        }
        return $year;
    }

    /**
     * Get year in Buddhist Era
     * @param int $y
     * @return int
     */
    public static function getYearBuddhist($y = null) {
        $year = isset($year) ? intval($year) : date('Y');
        return ($year + 543);
    }

}
